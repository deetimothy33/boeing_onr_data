# LOG FILE FORMAT
Each line contains the following:
[pc,bhr,fc0,fc1,fc2,pc0,pc1,pc2,pc3,bhr_mispredict,lbr_from_0,lbr_to_0,lbr_from_1,lbr_to_1,...,lbr_from_15,lbr_to_15]

This corresponds to one pmu measurement.
PMU measurements occur at each branch.

bhr_mispredict register in order of (MSB==most resent) to (LSB==least resent)


# CODE
for(vector<log_data_t*>::iterator it=log_data_v.begin(); it!=log_data_v.end(); ++it){
	// origional log contents
	*file  << (*it)->pc << ",";
	*file  << (*it)->bhr << ",";
	*file  << (*it)->pmc.fc0 << ",";
	*file  << (*it)->pmc.fc1 << ",";
	*file  << (*it)->pmc.fc2 << ",";
	*file  << (*it)->pmc.pc0 << ",";
	*file  << (*it)->pmc.pc1 << ",";
	*file  << (*it)->pmc.pc2 << ",";
	*file  << (*it)->pmc.pc3 << ",";
	// additional log contents
	*file  << (*it)->bhr_mispredict << ",";
	// print all lbr in order 
	// from most resent
	// to least resent
	for(int i=0; i<16; i++){
		*file  << lbr_from((*it)->pmc.lbr, i) << ",";
		*file  << lbr_to((*it)->pmc.lbr, i);
		// do not put a ',' on the last item
		if(i!=15) *file << ",";
	}
	*file  << endl;

	//print_pmc(&((*it)->pmc));
}

