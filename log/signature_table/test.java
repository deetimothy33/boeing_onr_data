/**
 * Created by Guo on 12/3/17.
 */
import weka.classifiers.functions.SMO;
import weka.classifiers.trees.RandomForest;
import weka.core.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;


public class test {
    public static final int numOfAttribute = 9;

    public static void main(String[] args) {
        String trainFolder = "/Users/Guo/boeing_onr/log/signature_table/set0/reference/";
        String targetFolder = "/Users/Guo/boeing_onr/log/signature_table/set0/attack/";
        getClassify(trainFolder, targetFolder);

    }

    public static double getClassify(String trainFolder, String targetFolder) {
        // Read from training and target folder
        File[] trainFiles = getAllFiles(trainFolder);
        File[] targetFiles = getAllFiles(targetFolder);
        ArrayList[] trainData = new ArrayList[trainFiles.length];
        ArrayList[] targetData = new ArrayList[targetFiles.length];
        for (int i = 0; i < trainFiles.length; i++) {
            trainData[i] = getfile(trainFiles[i]);
        }
        for (int j = 0; j < targetFiles.length; j++) {
            targetData[j] = getfile(targetFiles[j]);
        }
        int numOfRecords = trainData[0].size();
        int numOfTraining = Math.min(trainFiles.length, targetFiles.length) / 2;


        // Create Attributes
        ArrayList<Attribute> atts = consAttributes();

        // start training with training and target data
        SMO[] trainListSMO = new SMO[numOfRecords];
        RandomForest[] trainListRF = new RandomForest[numOfRecords];
        for (int k = 0; k < numOfRecords; k++) {
            // get Instances of current signature table index
            Instances trainIns = TtoInstances(trainData, targetData, k, numOfTraining, atts);
            //System.out.println("transIns size: " + trainIns.size());
            int sum = trainIns.numInstances();
            // SVM classifier
            SMO svm = new SMO();
            try {
                svm.buildClassifier(trainIns);
                trainListSMO[k] = svm;
            } catch (Exception e) {
                System.out.println(e.fillInStackTrace());
            }
            // Random forest classifier
            RandomForest rf = new RandomForest();
            try {
                rf.buildClassifier(trainIns);
                trainListRF[k] = rf;
            } catch (Exception e) {
                System.out.println(e.fillInStackTrace());
            }
        }

        //Read in all instances need to be classified
        int count = 0;
        for (int i = numOfTraining; i < trainFiles.length; i++) {
            Instances target = StoInstance(getfile(trainFiles[i]), atts, "first");
            if (!check(target, trainListSMO, trainListRF)) {
                count++;
            }
        }
        System.out.println("Correct Rate for no bufferOverFlow: " + (double) count / (trainFiles.length - numOfTraining));
        count = 0;
        for (int i = numOfTraining; i < targetFiles.length; i++) {
            Instances target = StoInstance(getfile(targetFiles[i]), atts, "second");
            if (check(target, trainListSMO, trainListRF)) {
                count++;
            }
        }
        System.out.println("Correct Rate under bufferOverFlow: " + (double) count / (targetFiles.length - numOfTraining));
        return 0.0;
    }

    public static boolean check(Instances target, SMO[] trainListSMO, RandomForest[] trainListRF) {
        Double[] res = verifyFile(target, trainListSMO, trainListRF);
        int count = 0;
        for (int i = 0 ; i < target.numInstances(); i++) {
            if (res[i] == 1.0) {
                count++;
            }
        }
        double percent = (double) count / target.numInstances();
        if (percent > 0.7) {
            return true;
        } else {
            return false;
        }
    }

    public static Double[] verifyFile(Instances target, SMO[] trainListSMO, RandomForest[] trainListRF) {
        Double[] res = new Double[target.numInstances()];
        for (int i  = 0; i < target.numInstances(); i++) {
            //System.out.println("Current Attribute " + i);
            Instance curIns = target.instance(i);
            try {
                res[i] = trainListSMO[i].classifyInstance(curIns);
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        }
        return res;
    }

    public static ArrayList<Attribute> consAttributes () {
        // create attributes
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        Attribute a = new Attribute("a");
        Attribute b = new Attribute("b");
        Attribute c = new Attribute("c");
        Attribute d = new Attribute("d");
        Attribute e = new Attribute("e");
        Attribute f = new Attribute("f");
        Attribute h = new Attribute("h");
        Attribute i = new Attribute("i");

        FastVector my_nominal_values = new FastVector(1);
        my_nominal_values.addElement("first");
        my_nominal_values.addElement("second");
        Attribute label = new Attribute("label", my_nominal_values);
        atts.add(label);
        atts.add(a);
        atts.add(b);
        atts.add(c);
        atts.add(d);
        atts.add(e);
        atts.add(f);
        atts.add(h);
        atts.add(i);
        return atts;
    }


    public static Instances StoInstance(ArrayList<ArrayList<Double>> curFile, ArrayList<Attribute> atts, String c) {
        Instances adataset = new Instances("aDataSet", atts, curFile.size());
        adataset.setClassIndex(0);
        // construct inst
        Instance inst = new DenseInstance(numOfAttribute);
        inst.setDataset(adataset);
        inst.setValue(0, c);
        for (int j = 1; j < curFile.size(); j++) {
            for (int i = 1; i < numOfAttribute; i++) {
                inst.setValue(i, curFile.get(j).get(i - 1));
            }
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all training data and convert them into instances
     * @param trainFiles set of training data
     * @param targetFiles set of target data
     * @param index signature table index
     * @return Instances
     */
    public static Instances TtoInstances(ArrayList<ArrayList<Double>>[] trainFiles, ArrayList<ArrayList<Double>>[] targetFiles, int index, int numOfFile, ArrayList<Attribute> atts) {
        //System.out.println("numOfFiles: " + numOfFile);

        // Create dataset
        Instances adataset = new Instances("aDataSet", atts, numOfFile);
        adataset.setClassIndex(0);
        // Add instance
        for (int j = 0; j < numOfFile; j++) {
            ArrayList<ArrayList<Double>> firstFile = trainFiles[j];
            ArrayList<Double> curline = firstFile.get(index);

            // construct inst
            Instance inst = new DenseInstance(numOfAttribute);
            inst.setDataset(adataset);
            inst.setValue(0, "first");
            for (int i = 1; i < numOfAttribute; i++) {
                inst.setValue(i, curline.get(i - 1));
            }
            adataset.add(inst);
        }
        for (int j = 0; j < numOfFile; j++) {
            ArrayList<ArrayList<Double>> firstFile = targetFiles[j];
            ArrayList<Double> curline = firstFile.get(index);

            // construct inst
            Instance inst = new DenseInstance(9);
            inst.setDataset(adataset);
            inst.setValue(0, "second");
            for (int i = 1; i < numOfAttribute; i++) {
                inst.setValue(i, curline.get(i - 1));
            }
            // add inst
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all files in targe folder
     * @param folderName folder name
     * @return a set of files
     */
    public static File[] getAllFiles(String folderName) {
        File f = new File(folderName);
        return f.listFiles();
    }


    /**
     * Read data from a file
     * @param file file contains data
     * @return data in Arraylist
     */
    public static ArrayList<ArrayList<Double>> getfile(File file) {
        ArrayList<ArrayList<Double>> res = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] attributes = line.split(",");
                ArrayList<Double> list = new ArrayList<>();
                for (int i = 0; i < attributes.length - 1; i++) {
                    String cur = attributes[i];
                    if (cur.equals("-nan")) {
                        list.add(-1.0);
                    } else {
                        list.add(Double.parseDouble(cur));
                    }
                }
                res.add(list);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.getName());
        }
        return res;
    }
}
