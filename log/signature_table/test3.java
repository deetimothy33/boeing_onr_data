import weka.clusterers.EM;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Guo on 12/5/17.
 */
public class test3 {
    public static final int numOfAttribute = 8;

    public static void main(String[] args) {
        String trainFolder = "/Users/Guo/boeing_onr/log/signature_table/set1/reference/";
        String targetFolder = "/Users/Guo/boeing_onr/log/signature_table/set1/attack/";
        getClassify(trainFolder, targetFolder);

    }

    public static boolean getClassify(String trainFolder, String targetFolder) {
        // Read from training and target folder
        File[] trainFiles = getAllFiles(trainFolder);
        File[] targetFiles = getAllFiles(targetFolder);
        ArrayList[] trainData = new ArrayList[trainFiles.length];
        ArrayList[] targetData = new ArrayList[targetFiles.length];
        for (int i = 0; i < trainFiles.length; i++) {
            trainData[i] = getfile(trainFiles[i]);
        }
        for (int j = 0; j < targetFiles.length; j++) {
            targetData[j] = getfile(targetFiles[j]);
        }
        int numOfRecords = trainData[0].size();
        int numOfTraining = 70;


        // Create Attributes
        ArrayList<Attribute> atts = consAttributes();


        System.out.println("Start training with normal signature table ...");
        System.out.println("Expectation-maximization algorithm is applied in data clustering");
        // Train and build a set of clusterers
        long t1 = System.currentTimeMillis();
        EM[] clusterers = new EM[numOfRecords];
        for (int k = 0; k < numOfRecords; k++) {
            Instances trainIns = TtoInstances(trainData, k, numOfTraining, atts);
            String[] op = new String[2];
            op[0] = "-I";                 // max. iterations
            op[1] = "100";
            clusterers[k] = new EM();
            try {
                clusterers[k].setOptions(op);     // set the options
                clusterers[k].buildClusterer(trainIns);
                //System.out.println("Number of clusters: " + clusterers[k].numberOfClusters());
            } catch (Exception e) {
                System.out.println(e.getStackTrace());
            }
        }
        int[] validIndexes = new int[numOfTraining];
        for (int i = 0; i < numOfTraining; i++) {
            int numOfValidIndex = 0;
            Instances curInstances = StoInstance(trainData[i], atts);
            for (int k = 0; k < numOfRecords; k++) {
                Instance curIns = curInstances.instance(k);
                EM curClusterer = clusterers[k];
                try {
                    int index = curClusterer.clusterInstance(curIns);
                    // System.out.println("file " + i + " record " + k + " cluster " + index);
                    double[] prios = curClusterer.clusterPriors();
                    double maxProb = 0;
                    double maxIndex = 0;
                    for (int j = 0; j < prios.length; j++) {
                        if (maxProb < prios[j]) {
                            maxProb = prios[j];
                            maxIndex = j;
                        }
                    }
                    if (index == maxIndex) {
                        numOfValidIndex++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            validIndexes[i] = numOfValidIndex;
        }
        double[] limits = getlimits(validIndexes);
        long t2= System.currentTimeMillis();
        System.out.println("Training costs " + (t2 - t1)/1000 + " sec.");




        System.out.println("Start testing with normal data: ...");
        System.out.println("Number of normal signature tables: " + (trainData.length - numOfTraining));

        double avgTime = 0;
        int count = 0;
        for (int i = numOfTraining; i < trainData.length; i++) {
            long t3 = System.currentTimeMillis();
            int numOfValidIndex = 0;
            Instances curInstances = StoInstance(trainData[i], atts);
            for (int k = 0; k < numOfRecords; k++) {
                Instance curIns = curInstances.instance(k);
                EM curClusterer = clusterers[k];
                try {
                    int index = curClusterer.clusterInstance(curIns);
                    // System.out.println("file " + i + " record " + k + " cluster " + index);
                    double[] prios = curClusterer.clusterPriors();
                    double maxProb = 0;
                    double maxIndex = 0;
                    for (int j = 0; j < prios.length; j++) {
                        if (maxProb < prios[j]) {
                            maxProb = prios[j];
                            maxIndex = j;
                        }
                    }
                    if (index == maxIndex) {
                        numOfValidIndex++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // System.out.println((i - 25) + " Normal numOfValidIndex: " + numOfValidIndex);
            if (numOfValidIndex > limits[0] && numOfValidIndex < limits[1]) {
                //System.out.print("Not under attack!");
                count++;
            } else {
                //System.out.print("Under attack!");
            }
            avgTime += (System.currentTimeMillis() - t3);
        }

        System.out.println("Identification accuracy is : " + ((double)count / (trainData.length - numOfTraining)));

        System.out.println("******************************************************");
        System.out.println("Start testing with under attack data: ...");
        System.out.println("Number of under attack signature tables: " + (targetData.length));

        count = 0;
        for (int i = 0; i < targetData.length; i++) {
            long t4 = System.currentTimeMillis();
            int numOfValidIndex = 0;
            Instances curInstances = StoInstance(targetData[i], atts);
            for (int k = 0; k < numOfRecords; k++) {
                Instance curIns = curInstances.instance(k);
                EM curClusterer = clusterers[k];
                try {
                    int index = curClusterer.clusterInstance(curIns);
                    // System.out.println("file " + i + " record " + k + " cluster " + index);
                    double[] prios = curClusterer.clusterPriors();
                    double maxProb = 0;
                    double maxIndex = 0;
                    for (int j = 0; j < prios.length; j++) {
                        if (maxProb < prios[j]) {
                            maxProb = prios[j];
                            maxIndex = j;
                        }
                    }
                    if (index == maxIndex) {
                        numOfValidIndex++;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            // System.out.println("Under attack numOfValidIndex: " + numOfValidIndex);
            if (numOfValidIndex > limits[0] && numOfValidIndex < limits[1]) {
                //System.out.print("Not under attack!");
            } else {
                count++;
                //System.out.print("Under attack!");
            }
            avgTime += System.currentTimeMillis() - t4;
        }

        System.out.println("Average identfication time: " + avgTime / (trainData.length + targetData.length - numOfTraining) / 1000);
        System.out.println("Identification accuracy is : " + ((double)count / (targetData.length)));
        return true;

    }

    public static double[] getlimits(int[] array) {
        double mean = 0;
        for (int i = 0; i < array.length; i++) {
            mean += array[i];
        }
        mean = mean / array.length;
        double sd = 0;
        for (int i = 0; i < array.length; i++) {
            sd += (array[i] - mean) * (array[i] - mean);
        }
        sd = Math.sqrt(sd) / array.length;
        return new double[]{mean - 6 * sd, mean + 6 * sd};
    }

    public static ArrayList<Attribute> consAttributes () {
        // create attributes
        ArrayList<Attribute> atts = new ArrayList<Attribute>();
        Attribute a = new Attribute("a");
        Attribute b = new Attribute("b");
        Attribute c = new Attribute("c");
        Attribute d = new Attribute("d");
        Attribute e = new Attribute("e");
        Attribute f = new Attribute("f");
        Attribute h = new Attribute("h");
        Attribute i = new Attribute("i");

        atts.add(a);
        atts.add(b);
        atts.add(c);
        atts.add(d);
        atts.add(e);
        atts.add(f);
        atts.add(h);
        atts.add(i);
        return atts;
    }


    public static Instances StoInstance(ArrayList<ArrayList<Double>> curFile, ArrayList<Attribute> atts) {
        Instances adataset = new Instances("aDataSet", atts, curFile.size());
        //adataset.setClassIndex(0);
        // construct inst
        Instance inst = new DenseInstance(numOfAttribute);
        inst.setDataset(adataset);
        for (int j = 0; j < curFile.size(); j++) {
            for (int i = 0; i < numOfAttribute; i++) {
                inst.setValue(i, curFile.get(j).get(i));
            }
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all training data and convert them into instances
     * @param trainFiles set of training data
     * @param index signature table index
     * @return Instances
     */
    public static Instances TtoInstances(ArrayList<ArrayList<Double>>[] trainFiles,  int index, int numOfFile, ArrayList<Attribute> atts) {
        //System.out.println("numOfFiles: " + numOfFile);

        // Create dataset
        Instances adataset = new Instances("aDataSet", atts, numOfFile);
        //adataset.setClassIndex(0);
        // Add instance
        for (int j = numOfFile; j < numOfFile + 20; j++) {
            ArrayList<ArrayList<Double>> firstFile = trainFiles[j];
            ArrayList<Double> curline = firstFile.get(index);

            // construct inst
            Instance inst = new DenseInstance(numOfAttribute);
            inst.setDataset(adataset);
            for (int i = 0; i < numOfAttribute; i++) {
                inst.setValue(i, curline.get(i));
            }
            adataset.add(inst);
        }
        return adataset;
    }

    /**
     * Get all files in targe folder
     * @param folderName folder name
     * @return a set of files
     */
    public static File[] getAllFiles(String folderName) {
        File f = new File(folderName);
        return f.listFiles();
    }


    /**
     * Read data from a file
     * @param file file contains data
     * @return data in Arraylist
     */
    public static ArrayList<ArrayList<Double>> getfile(File file) {
        ArrayList<ArrayList<Double>> res = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] attributes = line.split(",");
                ArrayList<Double> list = new ArrayList<>();
                for (int i = 0; i < attributes.length - 1; i++) {
                    String cur = attributes[i];
                    if (cur.equals("-nan")) {
                        list.add(-1.0);
                    } else {
                        list.add(Double.parseDouble(cur));
                    }
                }
                res.add(list);
            }
            scanner.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.getName());
        }
        return res;
    }
}
